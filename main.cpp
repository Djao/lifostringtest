/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP, Ruby, Perl,
C#, VB, Swift, Pascal, Fortran, Haskell, Objective-C, Assembly, HTML, CSS, JS, SQLite, Prolog.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
#include <stdio.h>
#include <iostream>
#include <cstring>  

using namespace std;
string ConcatRemove(string s, string t, int k);
bool ConcatAssert(string s, string t, int k, bool r);
void RunTests();

int main()
{
    RunTests();
    
    return 0;
}

void RunTests() {
    bool tests = true;
    string s = "abc";
    string t = "def";
    int k = 6;
    bool r = true;
    
    tests = tests && ConcatAssert(s, t, k, r);
    
    s = "abc";
    t = "def";
    k = 5;
    r = false;
    tests = tests && ConcatAssert(s, t, k, r);
    
    s = "blablablabla";
    t = "blablabcde";
    k = 8;
    r = true;
    tests = tests && ConcatAssert(s, t, k, r);
    
    s = "blablablabla";
    t = "blablabcde";
    k = 7;
    r = false;
    tests = tests && ConcatAssert(s, t, k, r);
    
    s = "tab";
    t = "tab";
    k = 7;
    r = true;
    tests = tests && ConcatAssert(s, t, k, r);
    
    s = "tab";
    t = "tab";
    k = 5;
    r = false;
    tests = tests && ConcatAssert(s, t, k, r);
    
    string msg = (tests)?"All tests succeeded!":"Some tests failed!";
    cout<<msg<<endl;
}

bool ConcatAssert(string s, string t, int k, bool r) {
    return ConcatRemove(s, t, k) == ((r)?"yes":"no");
}

string ConcatRemove(string s, string t, int k) {
    int ls = s.length();
    int lt = t.length();
    bool r;
    if(k >= ls + lt) return "yes";
    int i;
    
    for (i = 0; i < ls && i < lt; i++) {
        if(s[i] != t[i]) break;
    }
    
    return (k % ((ls + lt - 2 * i) != 0?(ls + lt - 2 * i):2) == 0)?"yes":"no";
}
