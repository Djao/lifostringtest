# Question 4

## Description
Unit-test for [https://gitlab.com/Djao/lifostring](https://gitlab.com/Djao/lifostring)

## Live-Test
In this link [https://onlinegdb.com/7ZmDA1G6_](https://onlinegdb.com/7ZmDA1G6_) it's possible to run the solution online and test values for s, t and k. The 'r' is the expected sucess/failure.   

For example  
s = "abc"  
t = "def"  
k = 6  
r = true  

s = "abc"  
t = "def"  
k = 5  
r = false  

There are a few test cases already being tested, but more can be added.
